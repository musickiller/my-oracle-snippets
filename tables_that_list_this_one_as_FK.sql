SELECT table_name
FROM ALL_CONSTRAINTS
WHERE constraint_type = 'R' -- "Referential integrity"
  AND r_constraint_name IN
    ( SELECT constraint_name
      FROM ALL_CONSTRAINTS
      WHERE table_name = 'TABLE_NAME'
        AND constraint_type IN ('U', 'P') -- "Unique" or "Primary key"
    );
