SELECT UNIQUE 'ALTER TABLE '
|| 'TABLE_NAME' -- table name
|| ' disable CONSTRAINT '
|| CONSTRAINT_NAME
|| ';'
FROM USER_CONS_COLUMNS
WHERE table_name = 'TABLE_NAME'
AND constraint_name LIKE 'FK_%'
